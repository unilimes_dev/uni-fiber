// @ts-nocheck
import { Loader, WebGLRenderer } from 'three'
// @ts-ignore
import { GLTFLoader, DRACOLoader, MeshoptDecoder, KTX2Loader } from 'three-stdlib'
import { useLoader } from '@react-three/fiber'

let dracoLoader: DRACOLoader | null = null
const ktxLoader = new KTX2Loader()
ktxLoader.setTranscoderPath('https://raw.githubusercontent.com/mrdoob/three.js/dev/examples/js/libs/basis/')
const testRenderer = new WebGLRenderer()
ktxLoader.detectSupport(testRenderer)

function extensions(useDraco: boolean | string, useMeshopt: boolean, extendLoader?: (loader: GLTFLoader) => void) {
  return (loader: Loader) => {
    if (extendLoader) {
      extendLoader(loader as GLTFLoader)
    }
    if (useDraco) {
      if (!dracoLoader) {
        dracoLoader = new DRACOLoader()
      }

      dracoLoader.setDecoderPath(
        typeof useDraco === 'string' ? useDraco : 'https://www.gstatic.com/draco/versioned/decoders/1.4.3/'
      )
      ;(loader as GLTFLoader).setDRACOLoader(dracoLoader)
    }

    if (useMeshopt) {
      ;(loader as GLTFLoader).setMeshoptDecoder(
        typeof MeshoptDecoder === 'function' ? MeshoptDecoder() : MeshoptDecoder
      )
    }

    ;(loader as GLTFLoader).setKTX2Loader(ktxLoader)
  }
}

export function useGLTF<T extends string | string[]>(
  path: T,
  useDraco: boolean | string = true,
  useMeshOpt: boolean = true,
  extendLoader?: (loader: any) => void
): any {
  const gltf = useLoader(GLTFLoader, path, extensions(useDraco, useMeshOpt, extendLoader))
  return gltf
}

useGLTF.preload = (
  path: string | string[],
  useDraco: boolean | string = true,
  useMeshOpt: boolean = true,
  extendLoader?: (loader: GLTFLoader) => void
) => useLoader.preload(GLTFLoader, path, extensions(useDraco, useMeshOpt, extendLoader))

useGLTF.clear = (input: string | string[]) => useLoader.clear(GLTFLoader, input)
