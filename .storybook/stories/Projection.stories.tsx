import * as React from 'react'
import { withKnobs, boolean } from '@storybook/addon-knobs'

import { Setup } from '../Setup'
import {
  ProjectionGroup,
  Projection,
  useGLTF,
  OrbitControls as OrbitControlsComp,
  useModelDecorator,
  Environment,
} from '../../src'

import * as THREE from 'three'
import { OrbitControls } from 'three-stdlib'
import { JSXModel } from './components/JSXModel'
import { Group } from 'three'

export default {
  title: 'Misc/Projection',
  component: Projection,
  decorators: [withKnobs, (storyFn) => <Setup controls={false}> {storyFn()}</Setup>],
}

const config = [
  { id: 606, position: [-0.021446, 1.8, 12.3037], image: 'projection/pano_1.webp' },
  { id: 605, position: [1.91636, 1.8, 9.76974], image: 'projection/pano_2.webp' },
  { id: 467, position: [-1.77948, 1.8, 7.87745], image: 'projection/pano_3.webp' },
  { id: 463, position: [1.95578, 1.8, 6.13301], image: 'projection/pano_4.webp' },
  { id: 489, position: [-0.021446, 1.8, 3.26241], image: 'projection/pano_5.webp' },
  // { id: 488, position: [17.289580035664052, 1.5, 14.797812147143047], image: 'projection/488.jpg' },
]

// Example of array with custom settings to objects on scene >>>
const objectsToChanges = [
  {
    //  Template for change texture to Image >>>
    mesh: ['display_1', 'display_2', 'display_3', 'display_6', 'display_7', 'display_8'], // array with object names
    material: 'MeshBasicMaterial', // this param create new material for the object
    type: 'image',

    textures: [
      {
        type: 'map',
        url: './images/living-room-2.jpg',
      },
      //  Template for change texture to Image <<<
    ],
  },
  // {
  // //  Template for change texture to Video >>>
  // mesh: ['Big_Wall'], // array with object names
  // material: 'MeshBasicMaterial', // this param create new material for the object
  // type: 'video',
  // textures: [
  //   {
  //     type: 'map',
  //     url: './projection/video.mp4',
  //     texture_properties: {
  //       color: 0xff0000,
  //     },
  //     texture_params: {
  //       type: 'video',
  //     },
  //   },
  // ],
  //  Template for change texture to Video <<<
  // },
]
// Example of array with custom settings to objects on scene <<<

function ProjectionScene() {
  const model = useGLTF('projection/Room_main_124.glb')
  const [activeViewpoint, setActiveViewpoint] = React.useState(0)

  const [controls, setControls] = React.useState<OrbitControls>(null!)
  const _controls = React.useRef<OrbitControls>(null!)

  const [JSXModelScene, setJSXModelScene] = React.useState<any>(null!)
  const _JSXModel = React.useRef<Group>(null!)

  const wireframe = boolean('wireframe', false)
  const withAnimation = true

  // Decorate custom items
  useModelDecorator(objectsToChanges, _JSXModel.current)

  React.useEffect(() => {
    setControls(_controls.current)
  }, [_controls.current])

  React.useEffect(() => {
    setJSXModelScene(_JSXModel.current)
  }, [_JSXModel])

  // this useEffect for video autoplay
  React.useEffect(() => {
    if (model.scene) {
      const video = objectsToChanges.find((el) => el.type === 'video')
      if (video) {
        model.scene.traverse((el) => {
          if (video.mesh.includes(el.name) && el.material.map.image) {
            setTimeout(() => el.material.map.image.play(), 1000)
          }
        })
      }
    }
  }, [model.scene])

  return (
    <group>
      <OrbitControlsComp rotateSpeed={-1} ref={_controls} />

      <Environment preset={'apartment'} background={true} />

      <JSXModel ref={_JSXModel} />

      {controls ? (
        <ProjectionGroup
          wireframe={wireframe}
          animation={withAnimation ? { easing: 'outSine', speed: 1 } : undefined}
          controls={controls}
          // environment={model.scene} // We can pass the model object directly
          environmentRef={JSXModelScene}
          objectsWithCustomMaterials={objectsToChanges.map((el) => el.mesh).flat(2)}
        >
          {config.map((el, index) => (
            <Projection
              active={index === activeViewpoint}
              key={el.id}
              encoding={THREE.sRGBEncoding}
              position={el.position as THREE.Vector3Tuple}
              image={el.image}
            >
              {index !== activeViewpoint ? (
                <mesh onClick={() => setActiveViewpoint(index)}>
                  <sphereGeometry args={[0.1, 32, 32]} />
                  <meshBasicMaterial color={0xff} />
                </mesh>
              ) : null}
            </Projection>
          ))}
        </ProjectionGroup>
      ) : null}
    </group>
  )
}

export const ProjectionSt = () => (
  <React.Suspense fallback={null}>
    <ProjectionScene />
  </React.Suspense>
)

ProjectionSt.storyName = 'Default'
